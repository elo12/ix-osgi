package de.phillippartsch.ixosgi;

/**
 * Util class for all used constantss
 */
public class Constants {

    /**
     * this class should not be instantiated
     */
    private Constants() {
    }

    /**
     * id of plugin
     */
    public static final String ID_PLUGIN_IXOSGI = "de.phillippartsch.ix-osgi";

    /**
     * name of the plugin extension
     */
    public static final String NAME_EXTENSION = "ixOsgi";

    /**
     * name of group
     */
    public static final String NAME_GROUP_ELO = "elo";

    /**
     * name of configuration
     */
    public static final String NAME_CONFIGURATION_EMBEDDED = "embedded";

    /**
     * name of lib folder
     */
    public static final String NAME_FOLDER_LIBRARIES = "OSGI-INF/lib/";

    /**
     * Util class for folder names of output
     */
    public static class Output {
        /**
         * this class should not be instantiated
         */
        private Output() {
        }

        /**
         * Name of the indexserver plugin jarkata folder
         */
        public static final String IX_PLUGIN_JAKARTA_FOLDER = "IX Plug-ins - jakarta";

        /**
         * Name of the indexserver plugin javax folder
         */
        public static final String IX_PLUGIN_JAVAX_FOLDER = "IX Plug-ins - javax";
    }
}
