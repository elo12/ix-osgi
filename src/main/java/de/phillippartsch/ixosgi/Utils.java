package de.phillippartsch.ixosgi;

import org.gradle.api.Project;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.jvm.tasks.Jar;

/**
 * several utility functions
 */
public class Utils {

    /**
     * this class should not be instantiated
     */
    private Utils() {
    }

    /**
     * get get ixOsgi extension
     *
     * @param project the project
     * @return ixOsgi extension
     */
    public static IxOsgiExtension getExtension(Project project) {
        return project.getExtensions().getByType(IxOsgiExtension.class);
    }

    /**
     * try to get a configured jar task
     *
     * @param project the project
     * @return jar task, null if no jar task is found
     */
    public static Jar getJarTask(Project project) {
        return (Jar) project.getTasks().findByName(JavaPlugin.JAR_TASK_NAME);
    }

    /**
     * get Bundle-SymbolicName
     * defaults to "project.group" + "." + "project.name"
     * can be overwritten by config
     *
     * @param project the project
     * @return Bundle-SymbolicName
     */
    public static String getBundleSymbolicName(Project project) {
        IxOsgiExtension extension = Utils.getExtension(project);

        return extension.getBundleSymbolicName()
                .orElse(project.getGroup() + "." + project.getName()).get();
    }

    /**
     * get Bundle-Activator
     * defaults to "Bundle-SymbolicName" + ".Activator"
     * can be overwritten by config
     *
     * @param project the project
     * @return Bundle-Activator
     */
    public static String getBundleActivator(Project project) {
        IxOsgiExtension extension = Utils.getExtension(project);

        return extension.getBundleActivator()
                .orElse(getBundleSymbolicName(project) + ".Activator").get();
    }
}
