/**
 * Copyright 2023 - Phillip Partsch <elo@phillipppartsch.de>
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.phillippartsch.ixosgi.task;

import de.elo.ix.client.IXConnection;
import de.phillippartsch.ixosgi.Utils;
import org.gradle.api.tasks.TaskAction;

/**
 * Gradle task implementation
 */
public abstract class UninstallPluginTask extends GeneralPluginTask {

    /**
     * default constructor
     */
    public UninstallPluginTask() {
    }

    /**
     * uninstall osgi bundle
     */
    @TaskAction
    public void uninstallOsgiBundle() {
        this.bundleSymbolicName = Utils.getBundleSymbolicName(getProject());

        getLogger().lifecycle("Prepare to uninstall plugin={}", bundleSymbolicName);
        IXConnection ixConnection = createIxConnection();
        uninstallBundle(bundleSymbolicName, ixConnection);
        closeIxConnection(ixConnection);
    }
}
