/**
 * Copyright 2023 - Phillip Partsch <elo@phillipppartsch.de>
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.phillippartsch.ixosgi.task;

import de.elo.ix.client.IXConnection;
import de.phillippartsch.ixosgi.Utils;
import org.gradle.api.Task;
import org.gradle.api.tasks.TaskAction;

import java.nio.file.Paths;

/**
 * Gradle task implementation
 */
public abstract class InstallPluginTask extends GeneralPluginTask {

    /**
     * default constructor
     */
    public InstallPluginTask() {
    }

    /**
     * deploy osgi bundle to ix
     */
    @TaskAction
    public void deployOsgiBundle() {
        bundleSymbolicName = Utils.getBundleSymbolicName(getProject());

        Task jarTask = Utils.getJarTask(getProject());
        if (jarTask == null) {
            throw new IllegalArgumentException("No jar task was found in the project. The ix-osgi-deploy plugin depends on jar task");
        }

        String jarTaskOutput = jarTask.getOutputs().getFiles().getAsPath();
        getLogger().lifecycle("File={} is present", jarTaskOutput);
        if (!Paths.get(jarTaskOutput).toFile().exists()) {
            throw new IllegalArgumentException("Output=" + jarTaskOutput + " of jar task does not exist");
        }

        getLogger().lifecycle("Prepare to deploy plugin={}", bundleSymbolicName);

        IXConnection ixConnection = createIxConnection();
        deployPlugin(jarTaskOutput, bundleSymbolicName, ixConnection);
        closeIxConnection(ixConnection);
    }
}
