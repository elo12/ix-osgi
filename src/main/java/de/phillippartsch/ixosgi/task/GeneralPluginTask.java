/**
 * Copyright 2023 - Phillip Partsch <elo@phillipppartsch.de>
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.phillippartsch.ixosgi.task;

import byps.RemoteException;
import de.elo.ix.client.IXConnFactory;
import de.elo.ix.client.IXConnection;
import de.phillippartsch.ixosgi.Constants;
import de.phillippartsch.ixosgi.IxOsgiExtension;
import de.phillippartsch.ixosgi.Utils;
import org.gradle.api.DefaultTask;

import java.io.FileInputStream;

/**
 * Gradle task implementation
 */
public abstract class GeneralPluginTask extends DefaultTask {

    /**
     * default constructor
     */
    public GeneralPluginTask() {
        this.setGroup(Constants.NAME_GROUP_ELO);
    }

    /**
     * name of the bundle
     */
    protected String bundleSymbolicName;

    /**
     * create a new connection to indexserver
     *
     * @return ix connection
     */
    protected IXConnection createIxConnection() {
        var extension = Utils.getExtension(getProject());

        String url = extension.getUrl().get();
        String username = extension.getUsername().orElse("Administrator").get();
        String password = extension.getPassword().orElse("elo").get();

        IXConnFactory ixConnFactory = new IXConnFactory(url, "de.phillippartsch.ixosgi", "1.0");
        IXConnection ixConnection;
        try {
            ixConnection = ixConnFactory.create(username, password, "", null);
        } catch (RemoteException e) {
            getLogger().error("Can not connect to ix={} with user={} message={}", url, username, e.getLocalizedMessage());
            throw new RuntimeException(e);
        }

        getLogger().lifecycle("Successfully connected to ix={} with user={}", url, username);
        validateIxMajorVersion(ixConnection);
        return ixConnection;
    }

    /**
     * close ix connection
     *
     * @param ixConnection active ix connection
     */
    protected void closeIxConnection(IXConnection ixConnection) {
        getLogger().lifecycle("Close ix connection");
        ixConnection.close();
    }

    private void validateIxMajorVersion(IXConnection ixConnection) {
        IxOsgiExtension extension = Utils.getExtension(getProject());

        Integer configuredIxMajorVersion = extension.getIxMajorVersion().get();
        Integer connectedIxMajorVersion = ixConnection.getMajorVersion();

        if (configuredIxMajorVersion != connectedIxMajorVersion) {
            getLogger().error("IX major version differs! You configured the plugin to ixMajorVersion={} the ix you connected to has version={}.",
                    configuredIxMajorVersion, connectedIxMajorVersion);
            getLogger().error("Consider so set ixMajorVersion to {}",
                    connectedIxMajorVersion);
        }
    }

    /**
     * deploy configured osgi bundle
     *
     * @param path               path to jar file on the filesystem
     * @param bundleSymbolicName name of the bundle
     * @param ixConnection       valid ix connection
     */
    protected void deployPlugin(String path, String bundleSymbolicName, IXConnection ixConnection) {
        stopBundle(bundleSymbolicName, ixConnection);
        uninstallBundle(bundleSymbolicName, ixConnection);
        uploadBundle(path, bundleSymbolicName, ixConnection);
        startBundle(bundleSymbolicName, ixConnection);
    }

    /**
     * stop the plugin
     *
     * @param bundleSymbolicName name of the bundle
     * @param ixConnection       valid ix connection
     */
    private void stopBundle(String bundleSymbolicName, IXConnection ixConnection) {
        getLogger().lifecycle("Stop plugin={}", bundleSymbolicName);
        try {
            long pluginId = ixConnection.getPluginService().getPlugin(bundleSymbolicName).getId();
            ixConnection.getPluginService().stop(pluginId);
        } catch (RemoteException e) {
            getLogger().error("Can not stop plugin={}. This is not an error if plugin was not installed yet.", bundleSymbolicName);
        }
    }

    /**
     * uninstall the plugin
     *
     * @param bundleSymbolicName name of the bundle
     * @param ixConnection       valid ix connection
     */
    protected void uninstallBundle(String bundleSymbolicName, IXConnection ixConnection) {
        getLogger().lifecycle("Uninstall plugin={}", bundleSymbolicName);
        try {
            long pluginId = ixConnection.getPluginService().getPlugin(bundleSymbolicName).getId();
            ixConnection.getPluginService().uninstall(pluginId);
        } catch (RemoteException e) {
            getLogger().error("Can not uninstall plugin={}. This is not an error if plugin was not installed yet.", bundleSymbolicName);
        }
    }

    /**
     * upload the plugin
     *
     * @param path               path to jar file on the filesystem
     * @param bundleSymbolicName name of the bundle
     * @param ixConnection       valid ix connection
     */
    private void uploadBundle(String path, String bundleSymbolicName, IXConnection ixConnection) {
        getLogger().lifecycle("Upload plugin={} from path={}", bundleSymbolicName, path);
        try (var fileStream = new FileInputStream(path)) {
            ixConnection.getPluginService().upload(fileStream);
        } catch (Exception e) {
            getLogger().error("Can not install plugin={}", bundleSymbolicName);
            throw new RuntimeException(e);
        }
    }

    /**
     * try to start the plugin
     *
     * @param bundleSymbolicName name of the bundle
     * @param ixConnection       valid ix connection
     */
    private void startBundle(String bundleSymbolicName, IXConnection ixConnection) {
        getLogger().lifecycle("Start plugin={}", bundleSymbolicName);
        try {
            long pluginId = ixConnection.getPluginService().getPlugin(bundleSymbolicName).getId();
            ixConnection.getPluginService().start(pluginId);
        } catch (RemoteException e) {
            getLogger().error("Can not start plugin={}", bundleSymbolicName);
            onStartupError(e, bundleSymbolicName, ixConnection);
            throw new RuntimeException(e);
        }
    }

    /**
     * Checks error message. If error message says that bundle has unresolved dependencies, try to uninstall it
     *
     * @param e                  RemoteException to parse
     * @param bundleSymbolicName name of the bundle
     * @param ixConnection       valid ix connection
     */
    private void onStartupError(RemoteException e, String bundleSymbolicName, IXConnection ixConnection) {
        String errorMessage = e.getMessage();
        if (errorMessage.contains("BundleException")) {
            getLogger().error("Bundle={} has startup errors. Try to uninstall to prevent ix startup errors", bundleSymbolicName);
            uninstallBundle(bundleSymbolicName, ixConnection);
        }
    }
}
