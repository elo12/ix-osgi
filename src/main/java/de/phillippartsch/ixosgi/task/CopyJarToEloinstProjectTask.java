/**
 * Copyright 2023 - Phillip Partsch <elo@phillipppartsch.de>
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.phillippartsch.ixosgi.task;

import de.phillippartsch.ixosgi.Constants;
import de.phillippartsch.ixosgi.IxOsgiExtension;
import de.phillippartsch.ixosgi.Utils;
import org.gradle.api.DefaultTask;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Set;

/**
 * Gradle task implementation
 */
public abstract class CopyJarToEloinstProjectTask extends DefaultTask {

    /**
     * default constructor
     */
    public CopyJarToEloinstProjectTask() {
        this.setGroup(Constants.NAME_GROUP_ELO);
    }

    /**
     * create manifest for osgi bundle
     */
    @TaskAction
    public void createManifest() {
        IxOsgiExtension extension = Utils.getExtension(getProject());
        Integer ixMajorVersion = extension.getIxMajorVersion().get();
        String eloinstProjectName = extension.getCopyJarToEloinstProject().get();

        String outputDir;
        if (ixMajorVersion <= 20) {
            outputDir = Constants.Output.IX_PLUGIN_JAVAX_FOLDER;
        } else {
            outputDir = Constants.Output.IX_PLUGIN_JAKARTA_FOLDER;
        }

        Set<Project> otherProjects = getProject().getRootProject().getSubprojects();
        for (Project otherProject : otherProjects) {
            if (otherProject.getName().equals(eloinstProjectName)) {
                var jarTask = getProject().getTasks().getByName(JavaPlugin.JAR_TASK_NAME);
                copyJar(otherProject, outputDir, jarTask);
                return;
            }
        }
        throw new IllegalArgumentException("No project with name=" + eloinstProjectName + " found");

    }

    private void copyJar(Project other, String outputDir, Task jarTask) {
        File jarFile = jarTask.getOutputs().getFiles().getSingleFile();

        Path targetDir = Paths.get(other.getProjectDir().getPath());
        targetDir = targetDir.resolve("src").resolve(outputDir);

        try {
            getProject().getLogger().lifecycle("Delete content of {}", targetDir);
            deleteFolder(targetDir);
            getProject().getLogger().lifecycle("Create folder {}", targetDir);
            Files.createDirectories(targetDir);
            getProject().getLogger().lifecycle("Copy {} to {}", jarFile, targetDir);
            Files.copy(Paths.get(jarFile.toURI()), targetDir.resolve(jarFile.getName()));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * delete a folder and it's content
     * @param folder Path to folder
     * @throws IOException IOException
     */
    public void deleteFolder(Path folder) throws IOException {
        if (!Files.exists(folder)) {
            return;
        }

        Files.walk(folder)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
    }
}
