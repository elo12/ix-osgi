/**
 * Copyright 2023 - Phillip Partsch <elo@phillipppartsch.de>
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.phillippartsch.ixosgi.task;

import de.phillippartsch.ixosgi.Constants;
import de.phillippartsch.ixosgi.IxOsgiExtension;
import de.phillippartsch.ixosgi.Utils;
import org.gradle.api.DefaultTask;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.artifacts.Dependency;
import org.gradle.api.artifacts.UnknownConfigurationException;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.tasks.TaskAction;
import org.gradle.jvm.tasks.Jar;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Gradle task implementation
 */
public abstract class CreateManifestTask extends DefaultTask {

    /**
     * dependencies always needed for bundle
     */
    private static final String[] REQUIRE_BUNDLE = {
            "de.elo.ix.client",
            "slf4j.api",
    };

    /**
     * dependencies needed for bundle with ix version 20
     */
    private static final String[] REQUIRE_BUNDLE_V20 = {
            "javax.servlet-api",
    };

    /**
     * dependencies needed for bundle with ix version 23
     */
    private static final String[] REQUIRE_BUNDLE_V23 = {
            "jakarta.servlet-api",
    };

    /**
     * dependencies needed for bundle with rest with ix version 20
     */
    private static final String[] REQUIRE_BUNDLE_REST_V20 = {
            "de.elo.ix.plugin.rest",
            "javax.ws.rs-api"
    };

    /**
     * dependencies needed for bundle with rest with ix version 23
     */
    private static final String[] REQUIRE_BUNDLE_REST_V23 = {
            "de.elo.ix.plugin.rest",
            "jakarta.ws.rs-api"
    };

    /**
     * default constructor
     */
    public CreateManifestTask() {
        this.setGroup(Constants.NAME_GROUP_ELO);
    }

    private String bundleSymbolicName;

    /**
     * create manifest for osgi bundle
     */
    @TaskAction
    public void createManifest() {
        this.bundleSymbolicName = Utils.getBundleSymbolicName(getProject());

        Map<String, String> manifestAttributes = new LinkedHashMap<>();

        manifestAttributes.putAll(getManifestDefaultAttributes());
        manifestAttributes.put("Bundle-SymbolicName", bundleSymbolicName);
        manifestAttributes.put("Bundle-Name", bundleSymbolicName);
        manifestAttributes.put("Bundle-Version", getProjectVersion());
        manifestAttributes.put("Bundle-Activator", Utils.getBundleActivator(getProject()));
        manifestAttributes.put("Require-Bundle", getRequireBundle());
        manifestAttributes.put("Bundle-ClassPath", getBundleClassPath());

        for (Jar jar : getProject().getTasks().withType(Jar.class)) {
            for (String key : manifestAttributes.keySet()) {
                if (jar.getManifest().getAttributes().containsKey(key)) {
                    getLogger().lifecycle("Existing Manifest attribute {}={}", key, jar.getManifest().getAttributes().get(key));
                    continue;
                }
                jar.getManifest().getAttributes().put(key, manifestAttributes.get(key));
                getLogger().lifecycle("Generated Manifest attribute {}={}", key, manifestAttributes.get(key));
            }
        }
    }

    private Map<String, String> getManifestDefaultAttributes() {
        var manifestAttributes = new HashMap<String, String>();
        manifestAttributes.put("Manifest-Version", "1.0");
        manifestAttributes.put("Bundle-ManifestVersion", "2");
        manifestAttributes.put("Bundle-RequiredExecutionEnvironment", "JavaSE-1.8");

        manifestAttributes.put("Built-By", System.getProperty("user.name"));
        manifestAttributes.put("Built-Date", new java.util.Date().toString());
        manifestAttributes.put("Build-Jdk", System.getProperty("java.version"));
        manifestAttributes.put("Build-OS", System.getProperty("os.name"));
        return manifestAttributes;
    }

    private String getBundleClassPath() {
        var extraLibs = getConfiguration(Constants.NAME_CONFIGURATION_EMBEDDED);
        if (extraLibs == null) {
            return "";
        }

        var resolvedDependencies = extraLibs.getResolvedConfiguration().getFiles().stream()
                .map(f -> Constants.NAME_FOLDER_LIBRARIES + f.getName())
                .toArray(String[]::new);

        if (resolvedDependencies.length == 0) {
            return "";
        }

        return String.join(",", resolvedDependencies) + ",.";
    }

    private Configuration getConfiguration(String configurationName) {
        try {
            return getProject().getConfigurations().getByName(configurationName);
        } catch (UnknownConfigurationException e) {
            getLogger().warn("Configuration={} not found.", configurationName);
            return null;
        }
    }

    private String getProjectVersion() {
        return getProject().getVersion().toString();
    }

    private Set<String> getCompileOnlyApiDependencies() {
        Set<String> dependencies = new HashSet<>();

        var compileOnlyApi = getConfiguration(JavaPlugin.COMPILE_ONLY_API_CONFIGURATION_NAME);
        if (compileOnlyApi == null) {
            return dependencies;
        }

        return compileOnlyApi.getDependencies().stream()
                .map(Dependency::getName)
                .collect(Collectors.toSet());
    }

    private String getRequireBundle() {
        IxOsgiExtension extension = Utils.getExtension(getProject());

        var compileOnlyDependencies = getCompileOnlyApiDependencies();
        Integer ixMajorVersion = extension.getIxMajorVersion().get();

        List<String> requiredBundles = new ArrayList<>();
        requiredBundles.addAll(List.of(REQUIRE_BUNDLE));

        boolean isRest = (compileOnlyDependencies.contains("rest") || compileOnlyDependencies.contains("rest-plugin"))
                && compileOnlyDependencies.contains("openapi-generator");

        if (ixMajorVersion == 20) {
            requiredBundles.addAll(List.of(REQUIRE_BUNDLE_V20));
            if (isRest) {
                requiredBundles.addAll(List.of(REQUIRE_BUNDLE_REST_V20));
            }
        } else if (ixMajorVersion == 23) {
            requiredBundles.addAll(List.of(REQUIRE_BUNDLE_V23));
            if (isRest) {
                requiredBundles.addAll(List.of(REQUIRE_BUNDLE_REST_V23));
            }
        }
        return String.join(",", requiredBundles);
    }
}
