/**
 * Copyright 2023 - Phillip Partsch <elo@phillipppartsch.de>
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.phillippartsch.ixosgi;

import de.phillippartsch.ixosgi.actions.ActionFactory;
import de.phillippartsch.ixosgi.task.CopyJarToEloinstProjectTask;
import de.phillippartsch.ixosgi.task.CreateManifestTask;
import de.phillippartsch.ixosgi.task.InstallPluginTask;
import de.phillippartsch.ixosgi.task.UninstallPluginTask;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.provider.Property;

/**
 * Configuration of the gradle plugin
 */
public class IxOsgiPlugin implements Plugin<Project> {

    /**
     * default constructor
     */
    public IxOsgiPlugin() {
    }

    @Override
    public void apply(Project project) {
        project.getExtensions().create(Constants.NAME_EXTENSION, IxOsgiExtension.class);

        // must be called before evaluation of plugin
        project.getPlugins().withId(Constants.ID_PLUGIN_IXOSGI, ActionFactory.createEmbeddedConfigurationAction(project));

        // this must be done in afterEvaluate since properties defined in an Extension are not available in apply
        project.afterEvaluate(this::onAfterEvaluate);
    }

    /**
     * Action to set up the plugin
     *
     * @param project the project
     */
    private void onAfterEvaluate(Project project) {
        IxOsgiExtension extension = Utils.getExtension(project);

        validateIxMajorVersion(extension);

        project.getPlugins().withId(Constants.ID_PLUGIN_IXOSGI, ActionFactory.createDependenciesAction(project));
        project.getPlugins().withId(Constants.ID_PLUGIN_IXOSGI, ActionFactory.createJavaCompatibilityAction(project));
        project.getPlugins().withId(Constants.ID_PLUGIN_IXOSGI, ActionFactory.createRepositoriesAction(project));

        project.getTasks().register("installIxPlugin", InstallPluginTask.class, task -> {
            task.dependsOn(JavaPlugin.JAR_TASK_NAME);
        });

        project.getTasks().register("uninstallIxPlugin", UninstallPluginTask.class);

        project.getTasks().register("createIxPluginManifest", CreateManifestTask.class, task -> {
            task.setEnabled(extension.getCreateManifest().getOrElse(true));
        });

        project.getTasks().register("copyJarToEloinstProject", CopyJarToEloinstProjectTask.class, task -> {
            if (extension.getCopyJarToEloinstProject().getOrNull() == null) {
                task.setEnabled(false);
            }
            task.dependsOn(JavaPlugin.JAR_TASK_NAME);
        });

        var jarTask = project.getTasks().getByName(JavaPlugin.JAR_TASK_NAME);
        jarTask.dependsOn("createIxPluginManifest");
    }

    /**
     * Validate if ixMajorVersion is properly set. Otherwise, throw exception
     * Validate if ixMajorVersion has supported value. Otherwise, throw exception
     *
     * @param extension the extension
     */
    private void validateIxMajorVersion(IxOsgiExtension extension) {
        Property<Integer> ixMajorVersion = extension.getIxMajorVersion();
        if (!ixMajorVersion.isPresent()) {
            throw new IllegalArgumentException("ixMajorVersion in ixOsgi is not set. Possible Values: 20, 23");
        } else if (ixMajorVersion.get() != 20 && ixMajorVersion.get() != 23) {
            throw new IllegalArgumentException("ixMajorVersion in ixOsgi has unsupported value. Possible Values: 20, 23");
        }
    }
}
