package de.phillippartsch.ixosgi.actions;

import org.gradle.api.Project;

/**
 * Factory class to create actions to configure plugin
 */
public class ActionFactory {

    /**
     * this class should not be instantiated
     */
    private ActionFactory() {
    }

    /**
     * factory method to create new ConfigureDependenciesAction
     *
     * @param project the project
     * @return instance of ConfigureDependenciesAction
     */
    public static ConfigureDependenciesAction createDependenciesAction(Project project) {
        return new ConfigureDependenciesAction(project);
    }

    /**
     * factory method to create new ConfigureEmbeddedConfigurationAction
     *
     * @param project the project
     * @return instance of ConfigureEmbeddedConfigurationAction
     */
    public static ConfigureEmbeddedConfigurationAction createEmbeddedConfigurationAction(Project project) {
        return new ConfigureEmbeddedConfigurationAction(project);
    }

    /**
     * factory method to set sourceCompatibility and targetCompatibility of project
     *
     * @param project the project
     * @return instance of ConfigureJavaCompatibilityAction
     */
    public static ConfigureJavaCompatibilityAction createJavaCompatibilityAction(Project project) {
        return new ConfigureJavaCompatibilityAction(project);
    }

    /**
     * factory method to create new ConfigureRepositoriesAction
     *
     * @param project the project
     * @return instance of ConfigureRepositoriesAction
     */
    public static ConfigureRepositoriesAction createRepositoriesAction(Project project) {
        return new ConfigureRepositoriesAction(project);
    }


}