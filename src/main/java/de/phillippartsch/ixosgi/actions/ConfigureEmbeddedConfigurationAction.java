package de.phillippartsch.ixosgi.actions;

import de.phillippartsch.ixosgi.Constants;
import de.phillippartsch.ixosgi.Utils;
import org.gradle.api.Action;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.jvm.tasks.Jar;

/**
 * Configuration of the embedded configuration
 */
class ConfigureEmbeddedConfigurationAction implements Action<Plugin> {

    private final Project project;

    ConfigureEmbeddedConfigurationAction(Project project) {
        this.project = project;
    }

    @Override
    public void execute(Plugin plugin) {
        project.getLogger().info("ConfigureEmbeddedConfigurationAction: add embedded configuration");

        Configuration embeddedConfiguration = project.getConfigurations().create(Constants.NAME_CONFIGURATION_EMBEDDED);
        project.getConfigurations().getByName(JavaPlugin.API_CONFIGURATION_NAME).extendsFrom(embeddedConfiguration);

        Jar jarTask = Utils.getJarTask(project);
        jarTask.into(Constants.NAME_FOLDER_LIBRARIES, spec -> {
            spec.from(embeddedConfiguration);
        });
    }
}
