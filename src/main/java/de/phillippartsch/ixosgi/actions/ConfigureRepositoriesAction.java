package de.phillippartsch.ixosgi.actions;

import de.phillippartsch.ixosgi.IxOsgiExtension;
import de.phillippartsch.ixosgi.Utils;
import org.gradle.api.Action;
import org.gradle.api.Plugin;
import org.gradle.api.Project;

/**
 * configure maven repositories
 */
class ConfigureRepositoriesAction implements Action<Plugin> {

    private static String eloRepoUrl = "https://s3.eu-de.cloud-object-storage.appdomain.cloud/mvn-dev-elo-com/";
    private final Project project;

    ConfigureRepositoriesAction(Project project) {
        this.project = project;
    }

    @Override
    public void execute(Plugin plugin) {
        IxOsgiExtension extension = Utils.getExtension(project);

        if (!extension.getAddRepository().orElse(true).get()) {
            project.getLogger().info("ConfigureRepositoriesAction: do not add repositories");
            return;
        }

        project.getRepositories().mavenCentral();

        project.getRepositories().maven(mavenRepo -> {
            mavenRepo.setUrl(eloRepoUrl);
            project.getLogger().info("Add elo repo url=", eloRepoUrl);
        });
    }
}
