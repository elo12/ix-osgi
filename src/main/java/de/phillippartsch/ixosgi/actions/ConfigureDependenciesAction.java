package de.phillippartsch.ixosgi.actions;

import de.phillippartsch.ixosgi.IxOsgiExtension;
import de.phillippartsch.ixosgi.Utils;
import org.gradle.api.Action;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.plugins.JavaPlugin;

/**
 * Configure the compileOnlyApi dependencies
 */
public class ConfigureDependenciesAction implements Action<Plugin> {

    /**
     * default dependencies for plugin with elo 20
     */
    public static final String[] DEPENDENCIES_V20 = {
            "org.apache.felix:org.apache.felix.main:7.0.5",
            "org.apache.felix:org.apache.felix.framework:7.0.5",
            "javax.servlet:javax.servlet-api:3.1.0",
            "org.slf4j:slf4j-api:1.7.36"
    };

    /**
     * default dependencies for plugin with elo 23
     */
    public static final String[] DEPENDENCIES_V23 = {
            "org.apache.felix:org.apache.felix.main:7.0.5",
            "org.apache.felix:org.apache.felix.framework:7.0.5",
            "jakarta.servlet:jakarta.servlet-api:5.0.0",
            "org.slf4j:slf4j-api:2.0.7"
    };

    /**
     * default dependencies for plugin with rest with elo 20
     */
    public static final String[] DEPENDENCIES_REST_V20 = {
            "javax.ws.rs:javax.ws.rs-api:2.1.1"
    };

    /**
     * default dependencies for plugin with rest with elo 23
     */
    public static final String[] DEPENDENCIES_REST_V23 = {
            "jakarta.ws.rs:jakarta.ws.rs-api:3.1.0"
    };

    private final Project project;

    /**
     * default constructor
     *
     * @param project the project
     */
    ConfigureDependenciesAction(Project project) {
        this.project = project;
    }

    @Override
    public void execute(Plugin plugin) {
        IxOsgiExtension extension = Utils.getExtension(project);

        if (!extension.getAddDependencies().orElse(true).get()) {
            project.getLogger().info("ConfigureDependenciesAction: do not add dependencies");
            return;
        }

        Integer ixMajorVersion = extension.getIxMajorVersion().get();
        Boolean isRest = extension.getAddRestDependencies().orElse(false).get();

        if (ixMajorVersion == 20) {
            addDependencies(DEPENDENCIES_V20);
            if (isRest) {
                addDependencies(DEPENDENCIES_REST_V20);
            }
        } else if (ixMajorVersion == 23) {
            addDependencies(DEPENDENCIES_V23);
            if (isRest) {
                addDependencies(DEPENDENCIES_REST_V23);
            }
        }
    }

    private void addDependencies(String[] dependencyNotations) {
        for (String dependencyNotation : dependencyNotations) {
            project.getDependencies().add(JavaPlugin.COMPILE_ONLY_API_CONFIGURATION_NAME, dependencyNotation);
        }
    }
}
