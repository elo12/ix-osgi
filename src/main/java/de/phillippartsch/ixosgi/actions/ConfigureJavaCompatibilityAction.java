package de.phillippartsch.ixosgi.actions;

import de.phillippartsch.ixosgi.IxOsgiExtension;
import de.phillippartsch.ixosgi.Utils;
import org.gradle.api.Action;
import org.gradle.api.JavaVersion;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.plugins.JavaPlugin;
import org.gradle.api.tasks.compile.JavaCompile;

/**
 * configure sourceCompatibility and targetCompatibility
 */
class ConfigureJavaCompatibilityAction implements Action<Plugin> {

    private final Project project;

    ConfigureJavaCompatibilityAction(Project project) {
        this.project = project;
    }

    @Override
    public void execute(Plugin plugin) {
        IxOsgiExtension extension = Utils.getExtension(project);

        if (!extension.getAddJavaCompatability().orElse(true).get()) {
            project.getLogger().info("ConfigureCompileJavaAction: do not add source and target compatibility");
            return;
        }

        Integer ixMajorVersion = extension.getIxMajorVersion().get();

        if (ixMajorVersion == 20) {
            setCompatability(JavaVersion.VERSION_15);
        } else if (ixMajorVersion == 23) {
            setCompatability(JavaVersion.VERSION_17);
        }
    }

    private void setCompatability(JavaVersion javaVersion) {
        project.getPlugins().withType(JavaPlugin.class, javaPlugin -> {
            project.getTasks().withType(JavaCompile.class, compileTask -> {
                compileTask.setSourceCompatibility(JavaVersion.toVersion(javaVersion).toString());
                compileTask.setTargetCompatibility(JavaVersion.toVersion(javaVersion).toString());
            });
        });
    }
}
