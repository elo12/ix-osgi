/**
 * Copyright 2023 - Phillip Partsch <elo@phillipppartsch.de>
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.phillippartsch.ixosgi;

import org.gradle.api.provider.Property;

/**
 * Configuration directive of gradle plugin
 */
public abstract class IxOsgiExtension {

    /**
     * default constructor
     */
    public IxOsgiExtension() {
    }

    /**
     * optional: generate the manifest for osi bundle
     * default: true
     *
     * @return boolean
     */
    abstract public Property<Boolean> getCreateManifest();

    /**
     * mandatory: major version of the elo indexserver
     *
     * @return 20 or 23
     */
    abstract public Property<Integer> getIxMajorVersion();

    /**
     * optional: other subproject (must be on the same level) to copy the jar to
     * example: example.base where subproject is configured as include "example:base"
     *
     * @return String
     */
    abstract public Property<String> getCopyJarToEloinstProject();

    /**
     * optional: set compatibility to JavaVersion.VERSION_15 when ixMajorVersion is 20, JavaVersion.VERSION_17 when ixMajorVersion is 23
     * default: true
     *
     * @return boolean
     */
    abstract public Property<Boolean> getAddJavaCompatability();

    /**
     * optional: add default repositories, mavenCentral and elo public maven
     * default: true
     *
     * @return boolean
     */
    abstract public Property<Boolean> getAddRepository();

    /**
     * optional: add compileOnlyApi dependencies. Depending on ixMajorVersion different dependencies are applied
     * default: true
     *
     * @return boolean
     */
    abstract public Property<Boolean> getAddDependencies();

    /**
     * optional: add compileOnlyApi dependencies needed for rest plugin. Depending on ixMajorVersion different dependencies are applied
     * To use this property addDependencies must be true
     * default: false
     *
     * @return true
     */
    abstract public Property<Boolean> getAddRestDependencies();

    /**
     * optional: Override of Bundle-SymbolicName in manifest
     * To use this property createManifest must be true
     * default: ""
     *
     * @return String
     */
    abstract public Property<String> getBundleSymbolicName();

    /**
     * optional: Override of Bundle-Activator in manifest
     * To use this property createManifest must be true
     * default: ""
     *
     * @return String
     */
    abstract public Property<String> getBundleActivator();


    /**
     * mandatory: url of indexserver
     *
     * @return url of indexserver, e.g. http://localhost:9090/ix-Archiv/ix
     */
    abstract public Property<String> getUrl();

    /**
     * optional: username with main administrator permissions
     * default: Administrator
     *
     * @return username as string, e.g. Administrator
     */
    abstract public Property<String> getUsername();

    /**
     * optional: password of username
     * default: elo
     *
     * @return password as string, e.g. elo
     */
    abstract public Property<String> getPassword();
}
