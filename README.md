# ix-osgi-deploy

This gradle plugin is designed to

* ...create a manifest...
* ...upload, install and start...
* ...uninstall/remove...

an indexserver-osgi plugin/bundle

# Licence

Copyright 2023 - 2024: Phillip Partsch <elo@phillipppartsch.de>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

# Disclaimer

This software is developed by Phillip Partsch.
Phillip Partsch is not an employee of ELO Digital Office GmbH or associated with ELO Digital Office GmbH in any kind.
This software is not a product of ELO Digital Office GmbH. Thus, ELO Digital Office GmbH will not support this software!
Support can be found creating an issue at gitlab: https://gitlab.com/elo12/ix-osgi-deployer/-/issues

# Usage

1. Include the plugin in the plugin directive of you build.gradle file.

```
plugins {
    id "de.phillippartsch.ix-osgi" version "2.0"
}
```

2. Configure the plugin in your build.gradle file

```
ixOsgi {
    ixMajorVersion = 23
    url = "http://localhost:9090/ix-Archiv/ix"
    username = "Administrator"
    password = "elo"
}
```

3. Run the gradle command to deploy the plugin

```
./gradlew installIxPlugin
```

You should see an output like this, if everything works as expected

```
PS C:\XXX> ./gradlew deployIxPlugin

> Task :createIxPluginManifest
Generated Manifest Attributes: {Build-OS=Windows 11, Bundle-ManifestVersion=2, Built-Dat...

> Task :installIxPlugin
File=C:\XXX\org.example.jar is present
Prepare to deploy plugin=org.example
Successfully connected to ix=http://localhost:9090/ix-Archiv/ix with user=Administrator
Stop plugin=org.example
Uninstall plugin=org.example
Upload plugin=org.example from path=:\XXX\org.example.jar
Start plugin=org.example
Close ix connection
```

# Project structure

The plugin makes some assumptions.

* Bundle-SymbolicName= group + "." + rootProject.name
* Class which extends PluginActivator is named Activator
* Class Activator is located at the package group + "." + rootProject.name

## Example

If your settings.gradle contains this

```
rootProject.name = "myplugin"
```

and your build.gradle contains this

```
group = "org.example"
```

your PluginActivator would look something like this

```
package org.example.myplugin;

import de.elo.ix.client.plugin.PluginActivator;
import org.osgi.framework.BundleContext;

public class Activator extends PluginActivator {
}
```

## Example 2

A full example project can be found here:

https://gitlab.com/elo12/ix-osgi-examples

## Hot Deployment

You can run your gradle command with the ``continuous`` (``t``) parameter. That way gradle will automatically check your
code
for changed and deploys the plugin every time the code changes.

```
./gradlew installIxPlugin -t
```

# Advanced configuration

In the new Version of the plugin, the plugin tries to configure as much as possible for the gradle build.
The Plugin will configure:

* dependencies: the default dependencies needed for plugin development (apache.felix, servlet-api, slf4j) depending on
  the configured ix major version
* configuration: embedded
* java: sourceCompatibility and targetCompatibility depending on the configured ix major version
* repositories: MavenCentral and public EloMaven

You can change all the default configuration in the ixOsgi extension.

Full overview about all possible configuration options with the default value.

```
ixOsgi {
    createManifest = true
    ixMajorVersion = null

    addJavaCompatability = true
    addRepository = true
    addDependencies = true
    addRestDependencies = false

    bundleSymbolicName = null
    bundleActivator = null

    url = null
    username = "Administrator"
    password = "elo"
}
```

## Mandatory

The plugin must be configured at least with these 2 properties.

### ixMajorVersion

Major version of the indexserver to run the plugin.
Possible values are: 20 or 23
For ix with version 21.X usage of 20 should be fine

### url

Url of the indexserver

## Optional

You can customize the behavior of the plugin by configuring these properties.

### createManifest

Typically, after the jar task, the manifest is created.
The manifest creations takes care, that all the required Bundle-*** properties are set properly.

### addJavaCompatability

This adds the compatability for the java bytecode.
For ixMajorVersion=20 VERSION_15 is used.
For ixMajorVersion=23 VERSION_17 is used.

```
compileJava {
    sourceCompatibility = JavaVersion.VERSION_XX
    targetCompatibility = JavaVersion.VERSION_XX
}
```

### addRepository

This adds 2 repositories to the build configuration.

* maven central
* elo public maven

### addDependencies

This adds the dependencies needed, to compile the bundle to the build configuration.
Depending on the ixMajorVersion different dependencies are added

* apache.felix
* servlet-api
* slf4j

### addRestDependencies

This adds the dependencies needed for a rest plugin, to compile the bundle to the build configuration.
Depending on the ixMajorVersion different dependencies are added

* ws.rs-api

### bundleSymbolicName

If you don't want to follow the suggested naming and package convention you can overwrite the default name here.

### bundleActivator

If you don't want to follow the suggested naming and package convention you can overwrite the default name here.

### username

The username to connect to the ix.

### password

The password to connect to the ix.

# Debug - Always on (recommended)

If you want to debug the plugin you must start your indexserver in debug mode.
You should alter the java startup options of your indexserver. You have to open the ``ELO-1w.exe`` file to alter the
java
options.

1. Navigate to your ELO-Server installation and navigate to the server bin folder
   e.g. ``C:\ELOprofessional\servers\ELO-1\bin``
2. Run the ``ELO-1.exe``
3. Goto the ``Java`` Tab
4. Enter ``Java Options``
5. Add the line ``-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5006`` at the end
3. Restart the ELO-1 Service
5. Create a debug configuration "Remote JVM Debug" in your IDE

```
-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5006
```

6. Enjoy debugging

## Hints

* We use the port 5006 instead of the default port 5005, since it makes things easier if we want to debug flows (which
  kind of always used 5005 for debug)
* ELO-1 might not be your server/service name. You have to choose the tomcat which runs the indexserver.

# Debug - One time (not recommended)

If you want to debug the plugin you must start your indexserver in debug mode.
You should create a powershell script to start the indexserver in debug mode. You can also do this from command line,
but with a script you don't have to repeat all the steps each time.

1. Navigate to your ELO-Server installation and create a file named ``debug.ps1``
2. Adapt the script content to your installation

```
   $env:JAVA_HOME="D:\ELOprofessional\java"
   $env:CATALINA_HOME="D:\ELOprofessional\servers\SERVERNAME"
   $env:JPDA_ADDRESS="0.0.0.0:5006"
   D:\ELOprofessional\servers\SERVERNAME\bin\catalina.bat jpda start
```

3. Stop the elo server
4. Start the script
5. Create a debug configuration "Remote JVM Debug" in your IDE

```
-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5006
```

6. Enjoy debugging

# Development

Feel free to create pull requests or issues if something goes wrong.
I know that it's bad practice to put libraries in a git repository directly, but since I can not use a custom maven
repository I have to do it that way.
